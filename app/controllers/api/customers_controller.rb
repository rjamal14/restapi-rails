module Api
  class CustomersController < ApplicationController
    before_action :set_customer, only: [:show, :update, :destroy]
    before_action :search_customer, only: %i[index]

    # GET /customers
    def index
      @customers = @customers.page(params[:page] || 1).per(params[:per_page] || 10)
                .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")

      serial_customers = @customers.map { |customer| CustomerSerializer.new(customer, root: false) }

      response_pagination(serial_customers, @customers, 'success get data')
    end

    # GET /customers/1
    def show
      response_success("success get data", CustomerSerializer.new(@customer, root: false))
    end

    # POST /customers
    def create
      @customer = Customer.new(customer_params)

      if @customer.save
        response_success("success save data", CustomerSerializer.new(@customer, root: false))
      else
        response_error("error save data", @customer)
      end
    end

    # PATCH/PUT /customers/1
    def update
      if @customer.update(customer_params)
        response_success("success update data", CustomerSerializer.new(@customer, root: false))
      else
        response_error("error update data", @customer)
      end
    end

    # DELETE /customers/1
    def destroy
      if @customer.destroy
        response_success("success delete data")
      else
        response_error("error delete data")
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def search_customer
        @customers = if params[:search].present? && params[:search] != '{search}'
                  Customer.search(params[:search])
                else
                  Customer
                end
        @customers = @customers.ransack(params[:q]).result
      end

      def set_customer
        @customer = Customer.find(params[:id])
      rescue StandardError
        response_error('Data tidak ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def customer_params
        params.require(:customer).permit(:name, :address, :phone)
      end
  end
end
