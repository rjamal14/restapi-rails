module Api
  class ProductsController < ApplicationController
    before_action :set_product, only: [:show, :update, :destroy]
    before_action :search_product, only: %i[index]

    # GET /products
    def index
      @products = @products.page(params[:page] || 1).per(params[:per_page] || 10)
                .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")

      serial_products = @products.map { |product| ProductSerializer.new(product, root: false) }

      response_pagination(serial_products, @products, 'success get data')
    end

    # GET /products/1
    def show
      response_success("success get data", ProductSerializer.new(@product, root: false))
    end

    # POST /products
    def create
      @product = Product.new(product_params)

      if @product.save
        response_success("success save data", ProductSerializer.new(@product, root: false))
      else
        response_error("error save data", @product)
      end
    end

    # PATCH/PUT /products/1
    def update
      if @product.update(product_params)
        response_success("success update data", ProductSerializer.new(@product, root: false))
      else
        response_error("error update data", @product)
      end
    end

    # DELETE /products/1
    def destroy
      if @product.destroy
        response_success("success delete data")
      else
        response_error("error delete data")
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def search_product
        @products = if params[:search].present? && params[:search] != '{search}'
                  Product.search(params[:search])
                else
                  Product
                end
        @products = @products.ransack(params[:q]).result
      end

      def set_product
        @product = Product.find(params[:id])
      rescue StandardError
        response_error('Data tidak ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def product_params
        params.require(:product).permit(:name, :price, :description, :image)
      end
  end
end
