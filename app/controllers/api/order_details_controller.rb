module Api
  class OrderDetailsController < ApplicationController
    before_action :set_order_detail, only: [:show, :update, :destroy]
    before_action :search_order_detail, only: %i[index]

    # GET /order_details
    def index
      @order_details = @order_details.page(params[:page] || 1).per(params[:per_page] || 10)
                .order_detail("#{params[:order_detail_by] || 'created_at'} #{params[:order_detail] || 'desc'}")

      serial_order_details = @order_details.map { |order_detail| OrderDetailSerializer.new(order_detail, root: false) }

      response_pagination(serial_order_details, @order_details, 'success get data')
    end

    # GET /order_details/1
    def show
      response_success("success get data", OrderDetailSerializer.new(@order_detail, root: false))
      
    end

    # POST /order_details
    def create
      @order_detail = OrderDetail.new(order_detail_params)

      if @order_detail.save
        response_success("success save data", OrderDetailSerializer.new(@order_detail, root: false))
      else
        response_error("error save data", @order_detail)
      end
    end

    # PATCH/PUT /order_details/1
    def update
      if @order_detail.update(order_detail_params)
        response_success("success update data", OrderDetailSerializer.new(@order_detail, root: false))
      else
        response_error("error update data", @order_detail)
      end
    end

    # DELETE /order_details/1
    def destroy
      if @order_detail.destroy
        response_success("success delete data")
      else
        response_error("error delete data")
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def search_order_detail
        @order_details = if params[:search].present? && params[:search] != '{search}'
                  OrderDetail.search(params[:search])
                else
                  OrderDetail
                end
        @order_details = @order_details.ransack(params[:q]).result
      end

      def set_order_detail
        @order_detail = OrderDetail.find(params[:id])
      rescue StandardError
        response_error('Data tidak ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def order_detail_params
        params.require(:order_detail).permit(:order_detail_id, :product_id, :qty, :price)
      end
  end
end
  