module Api
  class OrdersController < ApplicationController
    before_action :set_order, only: [:show, :update, :destroy]
    before_action :search_order, only: %i[index]

    # GET /orders
    def index
      @orders = @orders.page(params[:page] || 1).per(params[:per_page] || 10)
                .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")

      serial_orders = @orders.map { |order| OrderSerializer.new(order, root: false) }

      response_pagination(serial_orders, @orders, 'success get data')
    end

    # GET /orders/1
    def show
      response_success("success get data", OrderSerializer.new(@order, root: false))
    end

    # POST /orders
    def create
      @order = Order.new(order_params)

      if @order.save
        response_success("success save data", OrderSerializer.new(@order, root: false))
      else
        response_error("error save data", @order)
      end
    end

    # PATCH/PUT /orders/1
    def update
      if @order.update(order_params)
        response_success("success update data", OrderSerializer.new(@order, root: false))
      else
        response_error("error update data", @order)
      end
    end

    # DELETE /orders/1
    def destroy
      if @order.destroy
        response_success("success delete data")
      else
        response_error("error delete data")
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def search_order
        @orders = if params[:search].present? && params[:search] != '{search}'
                  Order.search(params[:search])
                else
                  Order
                end
        @orders = @orders.ransack(params[:q]).result
      end

      def set_order
        @order = Order.find(params[:id])
      rescue StandardError
        response_error('Data tidak ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def order_params
        params.fetch(:order, {})
                .permit(:customer_id, :date, :total, :status,
                        order_details_attributes: %i[
                         id order_id product_id qty price
                      ])
      end
  end
end

  