class UserProfileSerializer < ActiveModel::Serializer
  attributes :id, :name, :gender, :birth_date

  def name
    object.full_name
  end
end
