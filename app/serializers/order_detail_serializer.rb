class OrderDetailSerializer < ActiveModel::Serializer
  attributes :id, :product_id, :product_name, :qty, :price

  def product_name
    object.try(:product).name
  end
end
