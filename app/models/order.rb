class Order < ApplicationRecord
  extend Enumerize
  include PgSearch::Model
  pg_search_scope :search, against: %i[id total],
                           using: {
                             tsearch: {
                               prefix: true
                             }
                           }
  enumerize :status, in: %i[pending paid cancel], default: :pending

  has_many  :order_details, dependent: :destroy
  belongs_to :customer

  accepts_nested_attributes_for :order_details
end
