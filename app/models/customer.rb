class Customer < ApplicationRecord
  include PgSearch::Model
  pg_search_scope :search, against: %i[name phone],
                           using: {
                             tsearch: {
                               prefix: true
                             }
                           }
end
