Rails.application.routes.draw do
 
  use_doorkeeper do
    skip_controllers :authorizations, :applications, :authorized_applications
  end

  namespace :api do
    resources :users, only: %i[index create]
    resources :bookmarks
    resources :customers
    resources :orders
    resources :order_details
    resources :products
  end

  
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
