class CreateOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :orders do |t|
      t.belongs_to :customer, null: false, foreign_key: true
      t.datetime :date
      t.float :total
      t.string :status

      t.timestamps
    end
  end
end
